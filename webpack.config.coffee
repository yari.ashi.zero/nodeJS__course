# webpack = require 'webpack'
# c = -> console.log.apply console, arguments

module.exports = config =
    node:
        fs: "empty"
    context: __dirname
    cache: true
    debug: true
    entry:
        app: ['./src/entry.coffee']
    stats:
        colors: on
        reasons: on
    module:
        loaders: [
            {
                test: /\.coffee$/
                loaders: ['coffee-loader']
            }
        ]
    output:
        path: __dirname
        filename: "dist/app__packed.js"

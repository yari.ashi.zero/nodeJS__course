

c = -> console.log.apply console, arguments


root = document.getElementById 'root'

{c, _, React, React_DOM, rr, shortid, assign, keys} = require './boilerplate.coffee'

c 'React', React

{p, button, div, h1, h2, h3, h4, h5, h6, span, svg, circle, rect, ul, line, li, ol, code, a, input, defs, clipPath, linearGradient, stop, g, path, d, polygon, image, pattern, filter, feBlend, feOffset, polyline, feGaussianBlur, feMergeNode, feMerge, radialGradient, foreignObject, text, ellipse} = React.DOM


# another = rr



main = rr
    some_func: ->
        @setState
            dynamic: "BUTTON WAS CLICKED + #{@state.dynamic}"
    change_text: (e)->
        c 'e', e
        c 'current_target', e.currentTarget.value
        @setState
            dynamic: e.currentTarget.value
    getInitialState: ->
        dynamic: "I will change when you type in the box."
    render: ->
        div
            style:
                position: 'absolute'
                width: window.innerWidth
                height: window.innerHeight
                left: 2
                right: 2
                top: 2
                bottom: 2
                margin: 20
            ,
            h1
                style: {}
                ,
                "Hello and welcome to the [edit] app."
            p null,
                "Here is a paragraph element."
            input
                type: text
                placeholder: 'type something'
                onChange: @change_text
                ,
            p null,
                @state.dynamic
            button null, "some text"
            button
                # onClick: -> alert "I was clicked"
                onClick: @some_func
                ,
                "some button"


React_DOM.render main(), root

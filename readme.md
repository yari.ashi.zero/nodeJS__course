


#  COURSE :: NodeJS in the full stack context
 _J Wylie Cullick_
 _Feb 22, 2016_



## PART ONE  ::  let's build a simple client side app with React & Webpack

While NodeJS was originally intended as a server-side tool, with the evolution of its tooling ecosystem it now has relevance across the stack.  The premier example of this is Webpack, a tool for building client-side code bundles for apps which allows the arrangement of module dependencies as according to the CommonJS/NodeJS pattern, and allows for the inclusion of many NodeJS modules for use on the client-side. With NW.js and Electron, we can build desktop native apps that have access to the full NodeJS API.  These cannot be served across the web in the traditional manner of webapps, but must be distributed for user to manually install.

During the course we will setup a full stack system consisting of a NodeJS server serving a React application to the user. In PART ONE we will focus on setting up the development environment for the client-side (the React app), and get started adding some features.
